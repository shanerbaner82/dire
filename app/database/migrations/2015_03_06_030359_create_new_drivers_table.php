<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewDriversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('newdrivers', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('age');
            $table->string('dob');
            $table->string('dl');
            $table->string('comp_id')->index();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('newdrivers', function(Blueprint $table)
		{
			//
		});
	}

}
