<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversAlertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alert_driver', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('alert_id')->unsigned()->index();
            $table->foreign('alert_id')->refernces('id')->on('alerts')->onDelete('cascade');
            $table->integer('driver_id')->unsigned()->index();
            $table->foreign('driver_id')->refernces('id')->on('drivers')->onDelete('cascade');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('alert_driver', function(Blueprint $table)
		{
			//
		});
	}

}
