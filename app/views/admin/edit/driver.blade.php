@extends('layouts.admin')
@section('cont')
    <div class="panel panel-primary">
        <div class="panel-heading">Edit {{$driver->first_name}} {{$driver->last_name}}</div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="/admin/driver/{{$driver->id}}" role="form" data-toggle="validator" >
                <fieldset>
                    <?php echo Form::token() ?>
                    <div class="form-group">
                        <label for="fname" class="col-lg-2 control-label">First Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="fname" name="fname" value="{{$driver->first_name}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lname" class="col-lg-2 control-label">Last Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="lname" name="lname" value="{{$driver->last_name}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input type="email" class="form-control" id="email" name="email" value="{{$driver->email}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-lg-2 control-label">Cell Phone Number</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="phone" value="{{$driver->phone}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="dl" class="col-lg-2 control-label">Drivers License #</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="license" value="{{$driver->dl}}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="company_id" value="{{$company->id}}">
                </fieldset>
                <input type="submit" class="btn btn-lg btn-primary pull-right" value="Submit Changes">
            </form>
        </div>
    </div>
@stop