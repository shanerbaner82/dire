@extends('layouts.admin')
@section('cont')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Edit</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="/admin/vehicle/{{$vehicle->id}}" role="form" data-toggle="validator" id="newVehicleForm">
                <fieldset>
                    <?php echo Form::token() ?>
                    <div class="form-group">
                        <label for="year" class="col-lg-2 control-label">Year</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="year" name="year" value="{{$vehicle->year}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="make" class="col-lg-2 control-label">Make</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="make" name="make" value="{{$vehicle->make}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="model" class="col-lg-2 control-label">Model</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="model" name="model" value="{{$vehicle->model}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="plate" class="col-lg-2 control-label">License Plate</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="plate" value="{{$vehicle->license}}" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="state" class="col-lg-2 control-label">State</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="state" value="{{$vehicle->state}}">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="company_id" value="{{$company->id}}">
                </fieldset>
                <input type="submit" class="btn btn-lg btn-primary pull-right" value="Submit Changes"/>
            </form>
        </div>
    </div>

@stop