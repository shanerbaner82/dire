<form role="form" action="submit.php" id="payment-form" method="post" >
    <span class="payment-errors alert alert-danger text-center" style="display:none"></span>
    <fieldset>
        <legend style=margin-top:45px;">Card Details</legend>
        <div class="form-group">
            <label >Card Number</label>
            <input type="text" class="form-control" data-stripe="number" required placeholder="Debit/Credit Card Number">
        </div>
        <div class="form-group">
            <label class=" control-label" >Expiration Date</label>
            <div >
                <div class="row">
                    <div class="col-xs-6">
                        <select class="form-control col-sm-2" required data-stripe="exp-month">
                            <option>Month</option>
                            <option value="01">Jan (01)</option>
                            <option value="02">Feb (02)</option>
                            <option value="03">Mar (03)</option>
                            <option value="04">Apr (04)</option>
                            <option value="05">May (05)</option>
                            <option value="06">June (06)</option>
                            <option value="07">July (07)</option>
                            <option value="08">Aug (08)</option>
                            <option value="09">Sep (09)</option>
                            <option value="10">Oct (10)</option>
                            <option value="11">Nov (11)</option>
                            <option value="12">Dec (12)</option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <select class="form-control" required data-stripe="exp-year">
                            <option>Year</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label >Card CVV</label>
            <input type="text" class="form-control" required data-stripe="cvc" placeholder="Security Code">
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Please Note:</h3>
            </div>
            <div class="panel-body">
                Your card will be charged <span id="amount"></span> when you click "Pay Now".</br>
                Your account statement will show "<span id="statement"></span>"
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Pay Now</button>
    </fieldset>
</form>