@extends('layouts.admin')

@section('cont')

    <h1>Welcome!</h1>

    @foreach ($vehicles as $vehicle)
        <p>This is a vehicle: {{ $vehicle->year }}  {{ $vehicle->make }} {{ $vehicle->model }}, </p>
    @endforeach


@stop