@extends('layouts.admin')

@section('cont')

    <div class="col-md-8 col-xs-12">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Drivers: {{count($drivers)}}</h3>
                    <span class="pull-right">
                        <button class="btn btn-material-deep-orange-400 btn-xs " style="margin:0px;" data-toggle="modal" data-target="#addDriver">
                            <i class="fa fa-plus"> Add New</i>
                        </button>
                    </span>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <th>Name</th>
                            {{--<th>Alerts</th>--}}
                            <th>DL#</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        @foreach($drivers as $driver)
                            <tr>
                                <td>{{$driver->first_name}} {{$driver->last_name}}</td>
                                {{--<td>{{$driver->alerts['message']}}</td>--}}
                                <td>{{$driver->dl}}</td>
                                <td>{{$driver->phone}}</td>
                                <td>{{$driver->email}}</td>
                                <td>
                                    <button type="button"style="margin:0px;"  class="btn btn-material-light-blue-100 btn-xs btn-block"  onclick="editDriver({{$driver->id}})">Edit</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Vehicles: {{count($vehicles)}}</h3>
                <span class="pull-right">
                    <button class="btn btn-material-deep-orange-400 btn-xs" style="margin:0px;" data-toggle="modal" data-target="#addVehicle">
                        <i class="fa fa-plus"> Add New</i>
                    </button>
                </span>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-responsive">
                        <tr>
                            <th>Year</th>
                            <th>Make</th>
                            <th>Model</th>
                            <th>License</th>
                            <th>Action</th>
                        </tr>
                        @foreach($vehicles as $vehicle)
                            <tr>
                                <td>{{$vehicle->year}}</td>
                                <td>{{$vehicle->make}}</td>
                                <td>{{$vehicle->model}}</td>
                                <td>{{$vehicle->license}}</td>
                                <td>
                                    <button type="button" style="margin:0px;" class="btn btn-material-light-blue-100 btn-xs btn-block"  onclick="editVehicle({{$vehicle->id}})">Edit</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
    </div>
    </div>

    <div class="col-md-4 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Alerts: {{count($alerts)}}</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-responsive">
                    <tr>
                        <th>Driver's Name</th>
                        <th>Date/Time</th>
                        <th>Map</th>
                    </tr>
                        @foreach($alerts as $alert)
                        <tr>
                            <td>{{$alert->driver_name}}</td>
                            <td>{{date('m-d-Y', strtotime($alert->created_at))}}</td>
                            <td><a href="#">Link to Map</a></td>
                        </tr>
                        @endforeach
                </table>
                <form class="form-horizontal" method="POST" action="http://driveralertapp.com/alerts" role="form" data-toggle="validator">
                    <fieldset>
                        <legend>Add A New Alert</legend>
                        <?php echo Form::token() ?>
                        <div class="form-group">
                            <label for="year" class="col-lg-2 control-label">State</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="year" name="state"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="make" class="col-lg-2 control-label">Plate</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="make" name="plate"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="model" class="col-lg-2 control-label">Message</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="model" name="message"  required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="plate" class="col-lg-2 control-label">Lat</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="lat" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="col-lg-2 control-label">Long</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" name="long" >
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" class=" btn btn-xs btn-info"></button>
                </form>
            </div>
        </div>
    </div>

    </div>
    <div class="modal fade" id="addDriver" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="driver" role="form" data-toggle="validator" id="newDriverForm">
                        <fieldset>
                            <legend>Add A New Driver</legend>
                            <?php echo Form::token() ?>
                            <div class="form-group">
                                <label for="fname" class="col-lg-2 control-label">First Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="John" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lname" class="col-lg-2 control-label">Last Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Doe" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="someone@example.com" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-lg-2 control-label">Cell Phone Number</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="phone" placeholder="352-555-1212"required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dl" class="col-lg-2 control-label">Drivers License #</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="license" placeholder="R253123123ADASD">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="company_id" value="{{$company->id}}">
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material-deep-orange-400" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary btn-orange"  onclick="saveDriver()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addVehicle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="vehicle" role="form" data-toggle="validator" id="newVehicleForm">
                        <fieldset>
                            <legend>Add A New Driver</legend>
                            <?php echo Form::token() ?>
                            <div class="form-group">
                                <label for="year" class="col-lg-2 control-label">Year</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="year" name="year" placeholder="2012" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="make" class="col-lg-2 control-label">Make</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="make" name="make" placeholder="Ford" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="model" class="col-lg-2 control-label">Model</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="model" name="model" placeholder="F-150" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="plate" class="col-lg-2 control-label">License Plate</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="plate" placeholder="12345AWZ"required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="state" class="col-lg-2 control-label">State</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="state" placeholder="FL">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" name="company_id" value="{{$company->id}}">
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="newVehicle()">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        var saveDriver = function(){
            $('#newDriverForm').submit();
        };
        var newVehicle = function(){
            $('#newVehicleForm').submit();
        };
        var editDriver = function($id){
            window.location.href = "driver/"+$id+"/edit"
        };
        var editVehicle = function($id){
            window.location.href = "vehicle/"+$id+"/edit"
        };
    </script>
@stop