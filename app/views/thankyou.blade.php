@extends('layouts.default')

@section('cont')
    <div class="container well">

        <h1>Thank you!</h1>

        <h5>Your submission has been received successfully. We will contact you as quickly as possible.</h5>
    </div>

@stop