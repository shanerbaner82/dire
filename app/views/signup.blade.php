@extends('layouts.default')

@section('cont')
    <div class="container well" >
            <form class="form-horizontal" method="POST" action="/company" role="form" data-toggle="validator">

                <fieldset>
                    <legend>Sign Up</legend>
                    <?php echo Form::token() ?>
                    <div class="form-group">
                        <label for="company" class="col-lg-2 control-label">Company Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="company" name="company_name" placeholder="Company Name" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-lg-2 control-label">Your Title/Position</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputEmail" name="poc" placeholder="Title" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-lg-2 control-label">Your Email Address</label>
                        <div class="col-lg-10">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Your Email Address" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                <div class="form-group">
                    <label for="add1" class="col-lg-2 control-label">Address Line 1</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="add1" placeholder="Address Line 1"required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add2" class="col-lg-2 control-label">Address Line 2</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="add2" placeholder="Address Line 2">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-lg-2 control-label">City</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-lg-2 control-label">State</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="state" name="state" placeholder="State" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="zip" class="col-lg-2 control-label">Zip Code</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="zip" name="zip" placeholder="Zip Code" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-lg-2 control-label">Phone Number</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="years" class="col-lg-2 control-label">Years in Business</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="years"  name="years"placeholder="Years in Business" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="fleet" class="col-lg-2 control-label">Fleet Size</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="fleet" name="fleet" placeholder="Number of Vehicles" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                    <div class="form-group">
                        <label for="drivers" class="col-lg-2 control-label">Number of Drivers</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="drivers"  name="drivers" placeholder="Drivers" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-material-deep-purple">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
    </div>
    @stop