<ul class="admin-nav">
    <li  class="{{ Request::is('admin/company/accounts') ? 'active' : '' }}">
        <a href="/admin/company/accounts">Accounts</a>
    </li>
    <li class="{{ Request::is('admin/company/subscription') ? 'active' : '' }}">
        <a href="/">Subscription</a>
    </li>
    <li class="{{ Request::is('admin/company/magnets') ? 'active' : '' }}">
        <a href="/">Magnets</a>
    </li>
    <li class="{{ Request::is('admin/company/coupon') ? 'active' : '' }}">
        <a href="/">Coupon</a>
    </li>
    <li class="{{ Request::is('admin/company/history') ? 'active' : '' }}">
        <a href="/">Payment History</a>
    </li>
    <li class="{{ Request::is('admin/company/credit_card') ? 'active' : '' }}">
        <a href="/">Credit Card</a>
    </li>
    <li class="{{ Request::is('admin/company/cancel') ? 'active' : '' }}">
        <a href="/">Cancel</a>
    </li>
</ul>