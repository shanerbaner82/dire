<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title>Driver Alert</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ URL::asset('css/ripples.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/material.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/overridesadmin.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/snackbar.css') }}" rel="stylesheet">

    {{--<link href="{{ URL::asset('css/agency.css') }}" rel="stylesheet">--}}

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link href="{{ URL::asset('css/simple-sidebar.css') }}" rel="stylesheet">
    {{--<link href="https://bootswatch.com/sandstone/bootstrap.min.css" rel="stylesheet">--}}

</head>

<body>

{{--<div class="container-fluid shadow-z-2 clearfix" style="margin-top: 0px; padding:0px; background:#ff7043;color:#fff;">--}}
    <nav class="navbar navbar-default shadow-z-3" >
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="padding-left:20px;font-family: 'montserrat'; font-size:2.5em;" href="/admin/home">{{$company->name}}</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/admin/logout">Logout <i class="fa fa-sign-out"></i></a></li>

                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li><a href="#">Separated link</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    Driver Alert
                </a>
            </li>
            <li class="{{ Request::is('admin/home') ? 'active' : '' }}">
                <a href="/admin/home">Home</a>
            </li>
            <li class="{{ Request::is('admin/company') ? 'active' : '' }}">
                <a href="/admin/company/accounts">Company</a>
            </li>
            <li class="{{ Request::is('admin/contact') ? 'active' : '' }}">
                <a href="#">Contact</a>
            </li>

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="col-lg-12">
                @include('flash::message')
                @yield('cont')
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<script src="{{ URL::asset('js/ripples.min.js')}}"></script>
<script src="{{ URL::asset('js/material.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $.material.init();
    });
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('div.alert').not('.alert-important').delay(4000).slideUp(300);
</script>
</body>

</html>
