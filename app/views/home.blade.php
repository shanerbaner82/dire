@extends('layouts.default')

@section('cont')



    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome to Driver Alert</div>
                <div class="intro-heading">The 'How's My Driving' App</div>
                {{--<a href="#services" class="page-scroll btn btn-xl btn-material-deep-orange-700 btn-raised">Tell Me More</a>--}}
            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">What can we do for you?</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Better Reporting</h4>
                    <p class="text-muted">Keep tabs on drivers, know whats going on on the road, spot trouble drivers sooner.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-truck fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">How's My Driving in the 21st Century</h4>
                    <p class="text-muted">Gone are the days waiting on hold, waiting for reports, not knowing what is happening. Know as soon as a driver gets flagged what happened and COMMUNICATE with them instantly!</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-users fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Web Based Admin Interface</h4>
                    <p class="text-muted">Need a report on your worst drivers? When did that accident happen? Need to schedule a review with a driver? We got you covered!</p>
                </div>
            </div>
        </div>
    </section>



    {{--<!-- Clients Aside -->--}}
    {{--<aside class="clients">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img src="img/logos/envato.jpg" class="img-responsive img-centered" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img src="img/logos/designmodo.jpg" class="img-responsive img-centered" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img src="img/logos/themeforest.jpg" class="img-responsive img-centered" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-sm-6">--}}
                    {{--<a href="#">--}}
                        {{--<img src="img/logos/creative-market.jpg" class="img-responsive img-centered" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</aside>--}}

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" method="POST" action="/contactus" role="form" data-toggle="validator">
                        <?php echo Form::token() ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" name="name" required data-validation-required-message="Please enter your name.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" name="email" required data-validation-required-message="Please enter your email address.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" name="phone" required data-validation-required-message="Please enter your phone number.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" name="mymessage" required data-validation-required-message="Please enter a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl btn-material-deep-purple">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop