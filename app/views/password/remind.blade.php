@extends('layouts.default')

@section('cont')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-7" style="margin-top:100px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> Password Reset</div>
                    <div class="panel-body">
                        @if (Session::has('error'))
                            <p style="color:red;">{{Session::get('error')}}</p>
                         @elseif(Session::has('status'))
                            <p style="color:#0f9d58;">{{Session::get('status')}}</p>
                        @endif
                        <form class="form-horizontal"  method="POST"  role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">
                                    Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group last">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        Not Registered? <a href="/signup">Sign Up</a></div>
                </div>
            </div>
        </div>
    </div>
    <style>
        body {
            background: url("{{ URL::asset('img/bg.jpg')}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .panel-default {
            opacity: 0.9;
            margin-top:30px;
        }
        .form-group.last { margin-bottom:0px; }
        footer{background:#fff;}
    </style>
@stop