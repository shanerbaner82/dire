<?php

class DriverController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /driver
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /driver/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /driver
	 *
	 * @return Response
	 */
	public function store()
	{

		$driver = new Driver;
        $driver->first_name = Input::get('fname');
        $driver->last_name = Input::get('lname');
        $driver->phone = Input::get('phone');
        $driver->email = Input::get('email');
        $driver->dl = Input::get('license');
        $driver->company_id = Input::get('company_id');
        $driver->save();
        Flash::message('You successfully added a new driver!');
        return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /driver/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /driver/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $driver = Driver::find($id);
        return View::make('admin.edit.driver', ['driver' => $driver]);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /driver/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $driver = Driver::find($id);
        $driver->first_name = Input::get('fname');
        $driver->last_name = Input::get('lname');
        $driver->email = Input::get('email');
        $driver->dl = Input::get('license');
        $driver->phone = Input::get('phone');
        $driver->save();
        Flash::message('Driver successfully edited!');
		return Redirect::to('admin/home');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /driver/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function driverEdit($id)
    {
        $driver = Driver::find($id);
        return 'hello';
    }

}