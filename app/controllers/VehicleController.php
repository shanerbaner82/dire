<?php

class VehicleController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{


//		return View::make('myvehiclespage', [$vehicle]);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $vehicle = new Vehicle;
        $vehicle->year = Input::get('year');
        $vehicle->make = Input::get('make');
        $vehicle->model = Input::get('model');
        $vehicle->license = Input::get('plate');
        $vehicle->state = Input::get('state');
        $vehicle->company_id = Input::get('company_id');
        $vehicle->save();
        Flash::message('You successfully added a new vehicle!');
        return Redirect::back();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $vehicle = Vehicle::find($id);
        return View::make('admin.edit.vehicle', ['vehicle' => $vehicle]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $vehicle = Vehicle::find($id);
        $vehicle->year = Input::get('year');
        $vehicle->make = Input::get('make');
        $vehicle->model = Input::get('model');
        $vehicle->license = Input::get('plate');
        $vehicle->state = Input::get('state');
        $vehicle->save();
        Flash::message('Vehicles successfully edited');
        return Redirect::to('admin/home');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
