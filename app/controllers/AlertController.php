<?php

class AlertController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $alert = new Alert;
        $alert->state = Input::get('state');
        $alert->plate = Input::get('plate');
        $alert->message = Input::get('message');
        $alert->lat =Input::get('lat');
        $alert->long = Input::get('long');
//        $vehicle =  Vehicle::where('state', '=', $alert->state)->where('license', '=', $alert->plate)->first();
//        $driver =  $vehicle->driver->first();
        $alert->driver_id = 'Shane Rosenthal';
        $alert->save();

//        Driver::where('id','=',$driver->id)->first()->alerts()->attach($alert->id);
//        Company::where('id','=',$vehicle->company_id)->first()->alerts()->attach($alert->id);
        return ;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
