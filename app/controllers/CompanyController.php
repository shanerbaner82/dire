<?php


use DriveAlert\Mailers\UserMailers as Mailer;

class CompanyController extends \BaseController {


    protected $mailer;
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function accounts()
	{
        $user = Auth::user();
        $company =$user->company;
        $company_id = $company->id;
        $users = User::with('roles')->where('company_id', '=', $company_id)->get();
//        return $users;
		return View::make('admin.company.accounts', ['users' => $users]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $company = new Company;
        $company->name = Input::get('company_name');
        $company->fleet_size = Input::get('fleet');
        $company->drivers_size = Input::get('drivers');
        $company->poc = Input::get('poc');
        $company->email = Input::get('email');
        $company->add1 = Input::get('add1');
        $company->add2 = Input::get('add2');
        $company->city = Input::get('city');
        $company->state = Input::get('state');
        $company->zip = Input::get('zip');
        $company->phone = Input::get('phone');
        $company->years = Input::get('years');
        $company->save();

        $this->mailer->welcomeCompany($company);
        return Redirect::to('/');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public  function payment(){
        $user = Auth::user();
        $company =$user->company;
        $token = Input::get('stripeToken');
        $company->subscription('yearly')->create($token);
        return 'Done';
    }

}
