<?php namespace DriveAlert\Mailers;

use User;
use Company;
class UserMailers extends Mailer
{
    public function welcome(User $user)
    {
        $view = 'emails.welcome';
        $data = [];
        $subject = 'Welcome to Driver Alert';

        return $this->sendTo($user, $subject, $view, $data);
    }

    public function welcomeCompany(Company $company)
    {
        $view = 'emails.welcome';
        $data = [];
        $subject = 'Welcome to Driver Alert';

        return $this->sendTo($company, $subject, $view, $data);
    }
    public function contactForm($dataFromEmail)
    {
        $view = 'emails.contact';
        $data = $dataFromEmail;
        $subject = 'A Contact Us form was submitted on Driver Alert';

        return $this->sendToShane($subject, $view, $data);
    }
}

