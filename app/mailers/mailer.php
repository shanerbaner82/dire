<?php namespace DriveAlert\Mailers;
use Illuminate\Support\Facades\Mail;
abstract class Mailer {


    public function sendTo($user, $subject, $view, $data = [])
    {

        Mail::send($view, $data, function($message) use($user, $subject)
        {
            $message->to($user->email)->subject($subject);
        });
    }

    public function sendToShane($subject, $view, $data = [])
    {

        Mail::send($view, $data, function($message) use( $subject)
        {
            $message->to('srosenthal82@gmail.com')->subject($subject);
        });
    }
}
