<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::controller('password', 'RemindersController');
Route::get('/', function()
{
    return View::make('home');
});
Route::get('/login', 'SessionController@create');
Route::get('/signup', function()
{
    return View::make('signup');
});
Route::get('/thankyou', function()
{
    return View::make('thankyou');
});
Route::get('/admin/home', function()
{
    return View::make('admin.home');
})->before('auth');

Route::get('/play', function()
{
    $vehicle =  Vehicle::where('state', '=', 'FL')->where('license', '=', '12345')->first();
    $driver =  $vehicle->drivers->first();
    return $driver;
});


Route::get('/admin/home', function()
{
    $user = Auth::user();
    $company = $user->company;
    $drivers = $company->drivers;
    $vehicles = $company->vehicles;
    $alerts = $company->alerts()->get();
    return View::make('admin.home', ['vehicles' => $vehicles, 'user' => $user, 'company' => $company, 'drivers' => $drivers, 'alerts' => $alerts]);
})->before('auth');

Route::post('/session/store', 'SessionController@store');
Route::resource('session', 'SessionController');
Route::resource('company', 'CompanyController');
Route::post('/contactus', 'UserController@contact');
Route::resource('users', 'UserController');
Route::resource('alerts', 'AlertController');

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    if(Auth::check()){
        $user = Auth::user();
        $company =$user->company;
        View::share('company', $company);
    }
    Route::resource('driver', 'DriverController');
    Route::resource('vehicle', 'VehicleController');
    Route::post('/driver/{driver}', 'DriverController@update');
    Route::post('/vehicle/{vehicle}', 'VehicleController@update');
    Route::get('/company/accounts', 'CompanyController@accounts');
    Route::post('/company/payment', 'CompanyController@payment');
    Route::get('/logout', function() { Auth::logout();return Redirect::to('/login');});
});



