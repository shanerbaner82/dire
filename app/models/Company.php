<?php
use Laravel\Cashier\BillableInterface;
use Laravel\Cashier\BillableTrait;

/**
 * Created by PhpStorm.
 * User: Shane
 * Date: 3/5/2015
 * Time: 3:30 PM
 */

class Company extends \Eloquent implements BillableInterface {

    use BillableTrait;
    protected $table = 'companies';
    protected $fillable = [
        'name',
        'fleet_size',
        'drivers_size',
        'poc',
        'email',
        'add1',
        'add2',
        'city',
        'state',
        'zip',
        'phone',
        'years'
    ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    public function users(){
        return $this->hasMany('User');
    }
    public function drivers(){
        return $this->hasMany('Driver');
    }
    public function vehicles(){
        return $this->hasMany('Vehicle');
    }
    public function alerts(){
        return $this->hasManyThrough('Alert', 'Driver');
    }

}