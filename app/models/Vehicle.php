<?php
/**
 * Created by PhpStorm.
 * User: Shane
 * Date: 3/5/2015
 * Time: 3:30 PM
 */

class Vehicle extends \Eloquent {

    protected $table = 'vehicles';
    protected $fillable = ['*'];

    public function company(){
        return $this->belongsTo('Company');
    }
    public function drivers(){
        return $this->hasMany('Driver');
    }

}