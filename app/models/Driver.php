<?php
/**
 * Created by PhpStorm.
 * User: Shane
 * Date: 3/5/2015
 * Time: 3:30 PM
 */

class Driver extends \Eloquent {

    protected $table = 'drivers';
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'dl',
        'company_id'
    ];

    public function company(){
        return $this->belongsTo('Company');
    }

    public function alerts(){
        return $this->hasMany('Alert');
    }
    public function vehicles(){
        return $this->belngsTo('Vehicle');
    }
}