<?php
/**
 * Created by PhpStorm.
 * User: Shane
 * Date: 3/5/2015
 * Time: 3:30 PM
 */

class Alert extends \Eloquent {

    protected $table = 'alerts';
    protected $fillable = [
        'state',
        'plate',
        'message',
        'lat',
        'long',
        'driver_id',
        'driver_name'
    ];

    public function drivers(){
        return $this->belongsTo('Driver');
    }
    public function company(){
        return $this->belongsTo('Company');
    }

}